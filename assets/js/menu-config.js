jQuery(document).ready(function ($) {

    $(function () {
        $('#menu').slicknav({
            menuButton: '#menuButton',
            appendTo: 'nav',
            'afterOpen': function (trigger) {
                slicknavOpened(trigger);
            }
        });
    });

    $('button.hamburger').click(function () {
        $('button.hamburger').toggleClass('is-active');
    });

    function slicknavOpened(trigger) {

        var $trigger = $(trigger[0]);
        if ($trigger.hasClass('slicknav_btn')) {
            // this is the top-level menu/list opening.
            // we only want to trap lower-level/sublists.
            return;
        }
        // trigger is an <a> anchor contained in a <li>
        var $liParent = $trigger.parent();
        // parent <li> is contained inside a <ul>
        var $ulParent = $liParent.parent();
        // loop through all children of parent <ul>
        // i.e. all siblings of this <li>
        $ulParent.children().each(function () {
            var $child = $(this);
            if ($child.is($liParent)) {
                // this child is self, not is sibling
                return;
            }
            if ($child.hasClass('slicknav_collapsed')) {
                // this child is already collapsed
                return;
            }
            if (!$child.hasClass('slicknav_open')) {
                // could throw an exception here: this shouldn't happen
                // because I expect li to have class either slicknav_collapsed or slicknav_open
                return;
            }
            // found a sibling <li> which is already open.
            // expect that its first child is an anchor item.
            var $anchor = $child.children().first();
            if (!$anchor.hasClass('slicknav_item')) {
                return;
            }
            // close the sibling by emulating a click on its anchor.
            $anchor.click();
        });
    }

});
