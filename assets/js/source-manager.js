jQuery(document).ready(function ($) {

    /* Source manager check for users window height and picks appropriate background image or video from provided array */
    var sourceBlock = $('.js-source');

    sourceBlock.each(function () {
        /* parse array */
        var sourceData = JSON.parse($(this).attr('data-source'));
        /* check if this should be video or background image task */
        if(sourceData.hasOwnProperty("video")){
            if($(window).width() < 551){
                /* apply smallest cases (mobile)*/
                $(this).attr('poster', sourceData.video[0].posterSmall);
                $(this).attr('src', sourceData.video[0].sourceSmall);
            } else if ($(window).width() < 1025) {
                /* apply medium cases (tablet)*/
                $(this).attr('poster', sourceData.video[0].posterMedium);
                $(this).attr('src', sourceData.video[0].sourceMedium);
            } else{
                /* apply large cases (desktop)*/
                $(this).attr('poster', sourceData.video[0].posterFull);
                $(this).attr('src', sourceData.video[0].sourceFull);
            }
        }else if (sourceData.hasOwnProperty("background")) {
            if($(window).width() < 551){
                /* apply smallest cases (mobile)*/
                $(this).css("background-image", "url('"+ sourceData.background[0].sourceSmall + "')");
            } else if ($(window).width() < 1025) {
                /* apply medium cases (tablet)*/
                $(this).css("background-image", "url('"+ sourceData.background[0].sourceMedium +"')");
            } else{
                /* apply large cases (desktop)*/
                $(this).css("background-image", "url('"+ sourceData.background[0].sourceFull +"')");
            }
        }
    })
});
