# Default Slicing Template Proven Web Concepts  - Guidelines#

## Setup new slicing project ##
Create a branch for the new project: e.g. `checkout -b configurateme` where 'configurateme' is the new branch (make sure it is done from the **master** branch)

## Changes for each project ##
When there are changes that apply to every project, make them on the master branch, and merge the master branch to each project branch (command: `git merge --no-ff master` on the project branch)

## Naming conventions ##
Use lowercase names with words separated by dashes(-).

### BEM ###
When creating your classnames, try using BEM (`Block__Element--Modifier`) as much as possible. The benefits of using this naming system are that code will be much more readable and you can keep CSS specificity lower. Use double underscores and dashes in your naming so you can still use single dashes to make multi-word classnames. For example: `.page-header__logo--secondary`.

### CSS namespacing ###
To further increase the readability, modularity and decrease specificity of our (S)CSS code, make use of CSS namespacing/prefixing whenever applicable.

- `u-` utiility, mostly something that does only ONE thing, e.g. `.u-clearfix`, `.u-text-left`, etc.
- `c-` component e.g. `.c-btn`, `.c-btn--primary`, etc.
- `js-` javascript hook, do NOT use in (s)css, ONLY JS e.g. `.js-match-height`

### Partials ###
Always begin name of a partial SCSS file with an `_`, it won't be compiled via gulp and it won't show up in the css folder. But it can be used in the other `*.scss` files via the `@import` command.

#### Colors ####
In SCSS, define colors as much as possible as variables. Also usage of `$primary` and `$secondary` instead of just the color-name is encouraged. 

## Folder structure ##
In order for Gulp and CSScomb to function, you **MUST** stick to the following folder structure:

```
/PROJECT_ROOT
	/assets
		/css 
			only create folder, css files will be created by gulp
		/fonts
			only needed when using custom fonts (.woff, .eot, etc.)
		/img
			static images like icons, logos etc.
			use additional folders if this seems logical
		/js 
			all custom js files
		/lib
			all used libraries (js and css)
		/scss
			/base
				_reset.scss
				_variables.scss
				_default.scss
				_utility.scss
				_component.scss
				etc.
			/common
				_nav.scss
				_footer.scss
				etc.
			/contact (page-specific styles, if needed)
				_misc.scss
				etc.
			reset.scss - this file ONLY contains @imports's
			default.scss - this file ONLY contains @imports's
			contact.scss etc. (only if needed, else just use default.scss)
		/site-images
			all non-static images e.g. 'content'
			used only for development purposes
	/node_modules (this is created by npm)
	.gitignore
	compile.sh
	csscomb.json
	gulpfile.js
	package.json
	index.html
	homepage.html
	etc.
```

## Gulp ##
### Installation ###
Install all the node modules by running `npm install` in the command line.
For gulp specifically, install gulp globally using `npm install -g gulp`

### Usage ###
Convert *.scss, minify css and minify JS by running the `gulp` command in the command line.
Run the `gulp watch` command so the changed made to your scss and js files will be compiled automatically.

## CSScomb ##
To keep a consistent (s)css code style, we make use of CSScomb to keep our code nicely formatted.

### Installation ###
When running `npm install` the core CSScomb module should have already been installed. After it has been installed, you must replace the standard configuration file with the one provide in this repository `/csscomb.json`. Copy it to `/node_modules/csscomb/config` and replace the original file.

### Usage ###
To start using CSScomb, simply execute the bash file `compile.sh`. This first runs CSScomb and after that's finished, it runs gulp to compile your SCSS files.

Run CSScomb at least every time before you commit your files.

## SCSS ##
SCSS files **MUST** be placed in the assets/scss directory. 

### Techniques and best practises ###

#### Prefixes ####
Do not write any vendor prefixes in your SCSS code. They will be added automatically when running `gulp` whenever they are necessary.

#### Responsive (@media) styles ####
Do not cluster all your responsive (S)CSS in a single file. Instead, add your `@media` selectors directly to your main selector. This makes it much more maintainable. 

Also, place them directly after the main selector styles. Before any `::before`, `:hover` and child selectors. See example.

#### _reset.scss  ####
This file is used **ONLY** to reset the document styles. This is **NOT** used to set default styles. Use `_default.scss` for that purpose.

#### SCSS code example: ####
```scss
.block {
	position: relative;
	display: block;
	width: 90%;
	transition: background-color 0.15s;
	etc. 
	
	@media (max-width: 749px) {
		width: 100%;
		etc.
	}
	
	&:hover {
		background-color: red;
	}
	
	&::before {
		content: '';
		etc.
	}
	
	.block__element {
		position: relative;
		display: block;
		width: 100%;
		etc.
		
		&.block__element--modifier {
			background-color: blue;
			etc.
		}
	}
}
```

## Additonal information ##
These are in no particular order. Some of these may have already been mentioned elsewhere in this document.

- Use `index.html` just for navigation between the different pages, homepage should be `homepage.html`. This way, you don't need to fix the links in the navbar for every page.
- To compile SCSS into CSS, run `gulp` or `gulp watch` in the CLI form your project root.
- Vendor prefixes don't need to be written in scss, this is done by gulp.
- When compiled, css files will appear in assets/css. These are the files you link in your `<head>`.
- To run CSSCOMB, run `bash compile.sh` for your CLI in the project root.
- Some basic scss will already be provided (clearfix, container etc.), change values in these classes when needed.
- When using Slicknav in conjuction with `hamburgers.css`, set the speed to `400` to match with the `hamburgers.css` speed.
- Usage of flexbox is allowed and encouraged. It makes responsive development a whole lot easier compared to the usual method of using floats.
- All links **MUST** have a hover effect (only exception: site-logo).
- Use the Google Maps JS API instead of an iframe.
- Line-heights should always be unitless.
- Use `calc(100% / 3)` instead of `33.33333%`.
- Make use of HTML5 elements instead of the meaningless `<div>` whenever possible.
- Use background-image instead of actual img-tags for easy responsiveness (e.g. for the page-header).
- Only use ID's when absolutely necessary.

## Libraries ##
Some of the most used libraries are already included in this respository. Additionaly, here's a more complete overview of much used libraries:

- SlickNav (https://github.com/ComputerWolf/SlickNav)
- Pushy (https://github.com/christophery/pushy)
- OwlCarousel (https://github.com/OwlCarousel2/OwlCarousel2)
- SlickSlider (https://github.com/kenwheeler/slick/)
- FitVids.js (https://github.com/davatron5000/FitVids.js)
- Hamburgers (https://github.com/jonsuh/hamburgers)
- Remodal (https://github.com/VodkaBears/Remodal/)
- Lightbox2 (https://github.com/lokesh/lightbox2/)
- Select2 (https://github.com/select2/select2)
- matchHeight (https://github.com/liabru/jquery-match-height)